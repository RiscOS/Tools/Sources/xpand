; Copyright 2015 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
; > s.asmcall RCC 24-Mar-88
;
; This makes it possible to call code which scribbles on all the
; registers with a standard ARM procedure call, e.g. from C.
;
; If the code called also scribbles on lr, you must regain control
; by forcing it to branch to asmcall_exit.
;
; NB it relies on scribbling on memory addressed pc-relative, so it
; won't work if the code area is not writable.
;

r0  RN	0
r1  RN	1
r2  RN	2
r3  RN	3
r4  RN	4
ip  RN 12
lr  RN 14
pc  RN 15

    AREA |C$$code|, CODE, READONLY
    EXPORT  asmcall_call
    EXPORT  asmcall_exit

asmcall_savedregs
	DCD	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  ; 11 words for registers
asmcall_call
	;  Enter with r0: thing to call
	;	      r1, r2, r3: parameters for it

	ADR	ip, asmcall_savedregs
	STMIA	ip, {r4-lr}
	MOV	ip, r0
	MOV	r0, r1
	MOV	r1, r2
	MOV	r2, r3
	MOV	lr, pc
	MOV	pc, ip
asmcall_exit
	NOP		; 2 spurious instructions here in case the caller
	NOP		; forgets to allow for prefetch ...
	ADR	ip, asmcall_savedregs
	LDMIA	ip, {r4-lr}
	MOV	pc, lr
    END
